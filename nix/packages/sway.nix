{ config, pkgs, ... }:

{
	# Sway
	programs.sway = {
		enable = true;
		wrapperFeatures.gtk = true;
		extraPackages = with pkgs; [
			# Terminal
			foot

			# General
			sway-contrib.grimshot
			wl-clipboard
			libnotify
			autotiling
			swaylock
			waybar
			mako
			dmenu
			  
			# Themes
			glib
			gruvbox-dark-gtk
			gruvbox-dark-icons-gtk
			gnome.adwaita-icon-theme
		];
	};

	# Fonts
	fonts.fonts = with pkgs; [
		nerdfonts
	];
}

