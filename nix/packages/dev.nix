{ pkgs, ... }:

{
	# List packages installed in system profile.
	environment.systemPackages = with pkgs; [
		rustc
		cargo
		kotlin
		gradle
		nodejs
		go
		gcc
		jdk
		python3Minimal
		arduino-cli
		dotnet-sdk
	];
}

