{ pkgs, ... }:

{
	# List packages installed in system profile.
	environment.systemPackages = with pkgs; [
		# General
		networkmanagerapplet
		librewolf-wayland
		virt-manager
		libreoffice
		blanket
		krita
		imv
		mpv
		discord
		dbeaver

		# Games
		lutris
		minecraft
	];

	programs = {
		steam.enable = true;
		gamemode.enable = true;
	};
}

