{ pkgs, ... }:

{
	# List packages installed in system profile.
	environment.systemPackages = with pkgs; [
		# Utilities
		picocom
		openvpn
		pamixer
		libdrm
		yt-dlp
		unzip
		zip
		fzf
		git
		wget
		acpi
		btop
		tmux

		# Xdg
		xdg-user-dirs
		xdg-utils

		# FN-Keys
		brightnessctl
		playerctl

		# Editor
		neovim
	];

	# Shell
	programs.fish.enable = true;
	programs.starship = {
		enable = true;
		settings = {
			add_newline = false;

			character = {
  				success_symbol = "[>](bold green)";
				error_symbol = "[>](bold red)";
			};

			username.show_always = true;
		};
	};
}

