{ config, pkgs, ... }:

{
	imports =
	[
		<nixos-hardware/microsoft/surface>
		./hardware-configuration.nix
		./packages/sway.nix
		./packages/term.nix
		./packages/dev.nix
		./packages/gui.nix
	];

	# --- SYSTEM --- #
	# Use the systemd-boot EFI boot loader
	boot.loader.systemd-boot.enable = true;
	boot.loader.efi.canTouchEfiVariables = true;

	# Networking
	networking = {
		hostName = "lcm-bimtop";
		networkmanager.enable = true;
		useDHCP = false;
	};

	# Bluetooth
	hardware.bluetooth.enable = true;

	# Time zone
	time.timeZone = "Europe/Copenhagen";

	# Select internationalisation properties
	i18n.defaultLocale = "en_DK.UTF-8";
	console = {
		earlySetup = true;
		font = "${pkgs.terminus_font}/share/consolefonts/ter-122n.psf.gz";
		packages = with pkgs; [ terminus_font ];
		keyMap = "us";
	};

	# Accounts
	users.users.lcm = {
		shell = pkgs.fish;
		isNormalUser = true;
	};

	# Enable doas instead of sudo
	security.doas.enable = true;
	security.sudo.enable = false;

	# Configure doas
	security.doas.extraRules = [{
		users = [ "lcm" ];
		keepEnv = true;
		persist = true;
	}];

	# Allow non-free
	nixpkgs.config = {
		allowUnfree = true;
	};


	# --- SERVICES --- #
	# Sound
	security.rtkit.enable = true;
	services.pipewire = {
		enable = true;
		alsa.enable = true;
		alsa.support32Bit = true;
		pulse.enable = true;
	};

	# Logind
	services.logind.extraConfig = ''
		HandlePowerKey=ignore
		HandleLidSwitch=hibernate
		HandleLidSwitchDocked=hibernate
		HandleLidSwitchExternalPower=hibernate
	'';

	# OpenSSH
	services.openssh.enable = true;

	# TRIM
	services.fstrim.enable = true;

	# Print
	services.printing.enable = true;

	# Virtualisation
	virtualisation.libvirtd.enable = true;

	# Docker
	virtualisation.docker.enable = true;

	# System version
	system.stateVersion = "21.11";
}

