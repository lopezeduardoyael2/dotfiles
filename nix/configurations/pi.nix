{ config, pkgs, ... }:

{
	imports = 
	[
		./hardware-configuration.nix
		./packages/term.nix
		./packages/dev.nix
	];

	# --- SYSTEM --- #
	# Use the extlinux boot loader.
	boot.loader.grub.enable = false;
	boot.loader.generic-extlinux-compatible.enable = true;

	# Networking
	networking = {
		hostName = "ltd-bimserver";
		networkmanager.enable = true;
		useDHCP = false;
	}

	# Time zone
	time.timeZone = "Europe/Copenhagen";

	# Select internationalisation properties
	i18n.defaultLocale = "en_DK.UTF-8";
	console = {
		earlySetup = true;
		font = "${pkgs.terminus_font}/share/consolefonts/ter-122n.psf.gz";
		packages = with pkgs; [ terminus_font ];
		keyMap = "us";
	};

	# Accounts
	users.users.lcm = {
		shell = pkgs.fish;
		isNormalUser = true;
	};

	# Enable doas instead of sudo
	security.doas.enable = true;
	security.sudo.enable = false;

	# Configure doas
	security.doas.extraRules = [{
		users = [ "lcm" ];
		keepEnv = true;
		persist = true;
	}];


	# --- SERVICES --- #
	# Enable the OpenSSH daemon.
	services.openssh.enable = true;

	# System version
	system.stateVersion = "22.05";
}

