## File structure
<pre>
nix - main nix config files
    ∟ configurations
    ∟ packages

scripts - various scripts
    ∟ setup
    ∟ tty-colors

configs - contains dotfiles
    ∟ fish
    ∟ foot
    ∟ mako
    ∟ sway
        ∟ config
        ∟ scripts
        ∟ wallpapers
    ∟ swaylock
    ∟ user-dirs.dirs
</pre>


## Install
To install, run the ``setup`` script found in the ``scripts`` directory.


## NixOS channels
To move to the unstable channel:
```sh
nix-channel --add https://nixos.org/channels/nixos-unstable nixos
```


For the surface config add the hardware channel:
```sh
nix-channel --add https://github.com/NixOS/nixos-hardware/archive/master.tar.gz nixos-hardware
```


Then update:
```sh
# Update channels
nix-channel --update
```

