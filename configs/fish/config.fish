# --- Fish settings --- #
set -U fish_greeting


# --- Exports --- #
# Golang
set -gx PATH /usr/local/go/bin $PATH
set --export GOPATH ~/.go


# --- Sway --- #
# Only uncomment this line for Surface devices
proptest -M i915 -D /dev/dri/card0 103 connector 98 1

# Start Sway on login
if test (tty) = "/dev/tty1"
	sway
end

